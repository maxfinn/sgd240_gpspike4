﻿# Spike Report

## Outdoor Lighting

### Introduction

Unreal Engine has a large number of different light types, and many ways to shadow objects, with very different effects on performance (and different available options for different platforms).

Lights are a very complex topic, and good-looking lighting and fast lighting are both things which we don’t know how to do (let alone good-looking and fast lighting)! 

We have done indoor lighting, so it’s time for the outdoor stuff.

### Goals

1. Tech: What are the differences in the Light Mobility types (pros and cons)?
1. Skill: How to bake multiple Lighting Scenarios into a single level.
1. Knowledge: What is a Distance Field?

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Precomputed Lighting Scenarios](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/PrecomputedLightingScenarios/)
* [UE4 Answers - Lighting Scenario](https://answers.unrealengine.com/questions/512084/who-can-explain-of-lighting-scenario-feature.html)
* [Low Poly Tropical Pack](https://artemgamestudios.weebly.com/free-content.html)
* [Migrating Assets](https://docs.unrealengine.com/latest/INT/Engine/Content/Browser/UserGuide/Migrate/index.html)
* [Unreal Engine 4: Day night cycle using Blueprint](https://www.youtube.com/watch?v=BljC7-xMhHQ)
* [Distance Field Ambient Occlusion](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/DistanceFieldAmbientOcclusion/index.html)

### Tasks Undertaken

1. Using the Third Person character template as a base, remove the original geometry (Arena and Walkway) and add a Landscape in its place. Also add an AtmosphericFog actor if not present. We will name this level StaticLevel.
1. In the Foliage tool section, add a new Foliage Type and assign a tree mesh to it. Adjust Brush Size and Paint Density as required and add trees to the scene.
1. Create the sub-levels that will become our Lighting Scenarios
    1. Select the directional light, sky light, and sky sphere, and then open Window > Levels. Click on `Create New with Selected Actors...` in the Levels dropdown, and call this new sub-level StaticLevel_Morning.
    1. Use `Create New...` three times to create the remaining sub-levels, naming them StaticLevel_Midday, StaticLevel_Evening, and StaticLevel_Night respectively.
    1. Right click each of these sub-levels, and navigate to Lighting Scenario > Change to Lighting Scenario.
1. In each of the new sub-levels, add the directional light, sky light, and sky sphere. Set each of the directional and sky lights to static, and then adjust the directional light's rotation to suitable amounts for the different times of day. In the night Lighting Scenario, also set the directional light's intensity to zero.
1. Also ensure each sky sphere has its Directional Light Actor field set to the given Lighting Scenario's directional light.
1. Having now set up appropriate lighting with static mobility, we will do the same for Stationary.
    1. To expedite the process in this case, we can duplicate the StaticLevel and each of its sub-levels, renaming them with a Stationary prefix.
    1. Go into the Levels window once more, and in this case, use `Add existing...` to link each of the new sub-levels. Also set each of these to be Lighting Scenarios.
    1. Finally, go through each sub-level and change the directional and sky lights over to the Stationary mobility setting.
1. For the Movable level, we will duplicate StaticLevel once more, but in this case do not require the sub-levels.
    1. After setting the lights to Movable, open the Level Blueprint, and create a new Timeline. We will add a float track to this, with a range of 0.0 to 120.0, and the key values either side will be -270.0 and 90.0 to make a full day cycle.
    1. Link this as shown in figure 1 (note: a second float track was added for controlling the sun's brightness throughout the day (see figure 2)).
    1. Finally, in Project Settings > Rendering > Lighting, enable Generate Mesh Distance Fields and restart the editor. As we have added our trees as foliage, we will also need to enable Affect Distance Field Lighting in the foliage settings.


### What we found out

Unreal Engine 4 has three types of Light Mobility available to use. They were touched on briefly during the previous spike report, but they are Static, Stationary, and Movable. Static lighting is entirely fixed in place during the build process, and is the fastest of the three as it has no overhead. Static lighting can be of very high quality because you can bake the results of complex processes like Global Illumination in during the build process, which would otherwise be unfeasible to have in a game because of the time it takes to compute them. The next type of lighting is Stationary, and lights with this property are able to change their colour and brightness, but are still unable to move or rotate at runtime. This mobility level adds some verstility to the lights using it, allowing lights which can turn on and off for example. This does add some computational overhead however. Finally, there a Movable lights, which are able to adjust all of their values at runtime. These are the most computationally expensive to use, but give complete freedom in how they can be used. Build times are reduced while using Movable lights however, as they will not be baked into lightmaps.

The method for baking multiple Lighting Scenarios into a single level was described in detail in the previous section of Tasks Undertaken, but effectively it is done by creating multiple sub-levels which hold the lights responsible for each Lighting Scenario. The sub-levels will be linked up with the main level as Lighting Scenarios, and using Unload and Load Stream Level can be swapped in and out, while the levels geometry stays put in the main level (a less than elegant method of cycling through Lighting Scenarios can be seen in figure 4).

Distance Fields are a three dimensional grid structure, where each point stores a value representing the distance to the nearest mesh surface. In UE4's case, points inside of a mesh are given negative values. Though seemingly complex overall, these Distance Fields can be used to enable medium scale Ambient Occlusion when using Movable lights.

Initial setup up of this project proved to be somewhat difficult, with uncertainty as to what specific components of lighting should be separated into sub-levels and what should remain in the main level. Ultimately, I opted to extract only the directional and sky lights, and the sky sphere, whilst leaving the atmospheric fog in the main level.

Lighting in Unreal Engine 4 is evidently very powerful and capable, but it can be a very complex task to utilise all of the tools that it provides well.

### Open Issues/Risks

1. Oddly, shadow quality is highest in the Movable lighting scene, with shadows in both the Static and Stationary scenes being considerably lacking in detail.
    * My suspicion is that because I used such a large terrain initially, UE4 has probably had to stretch the lightmaps quite far to fit everything in and this has resulted in individual shadows being quite low in detail. This is despite all settings being at high or epic, so it is uncertain as to what the solution might be.
1. In the Movable scene, the sun is set to start pointing straight up, for midnight, and looping all the way back around again. It was expected that this would be a straightforward process and that as only rotation was being changed, initial conditions elsewhere should match up once the sun had fully looped around. However, the appearance of the sky goes through a very obvious jerky transition from a dark night sky to a pre-dawn-esque blue as the sun shifts past its midnight rotation.
    * Having switched various actors on and off as it cycled, I believe the issue lies within the Atmospheric Fog actor, but this has not led to a solution as I do not directly change any values within this actor, and it is unclear as to why it has this problem.
1. The Distance Field Ambient Occlusion does not appear particularly good looking to me, having applied it; the affect produces an odd ring of shadow which is based on the direction you face, as well as a strange ghosting effect on the trees when you move the camera back and forth past them.
    * I am unsure as to whether this is a true issue, or simply intended behaviour that doesn't look good in this particular scene. The distance fields them selves appear to be generating as expected (see figure 5)

### Recommendations

It would be of value to investigate the issues of the day cycle and fix the transition from one day to the next, as well as learning more about how shadows are managed in UE4 so as to restore quality to shadow maps on large terrains, as they are an important feature in most games with a large map.

### Appendix

Figure 1: The node setup for adjusting the sun's apparent location and lighting direction, as well as shifting its brightness to zero after it sets.

![Level Blueprint][MovableLights01]

Figure 2:  The curve used to manage light intensity.

![Timeline tracks][MovableLights02]

Figure 3: The editor will not load any sub-levels by default, resulting in a difference between what you see in-editor and in-play if not correctly set up.

![Skysphere strange behaviour][StaticLights01]

Figure 4: One method for switching between Lighting Scenarios, using the "M" key as a toggle (action mappings would likely make up part of a more tidy solution).

![FlipFlop sub-level switching][StaticLights02]

Figure 5: UE4's visualisation of the Distance Fields.

![Distance Field Visualisation][DistanceFields01]

[StaticLights01]: https://monosnap.com/file/mQQX4pmGh7GUgku0Zg7popDoBh1X2H.png "Skysphere behaving differently between the editor and when in play"
[MovableLights01]: https://monosnap.com/file/uJwvlAPnMMdto6G5N8UXrJHRsXL8UN.png "Blueprint for the day cycle"
[MovableLights02]: https://monosnap.com/file/JnvVyh04aMWSms1xmMP3dluQfKOJa3.png "Timeline tracks"
[StaticLights02]: https://monosnap.com/file/9dDPsav4UA7m1ylmlSVnbRfyMjAlKz.png "Lighting Scenario switching with FlipFlops"
[DistanceFields01]: https://monosnap.com/file/o6FN58h10ppzzPka1y3INK58OeXXJY.png "Distance Field visualisation"